using Discord.Net;
using Discord.WebSocket;
using AsciiCord.Bot.SlashCommands;


namespace AsciiCord.Bot
{
    public class AsciiCordBot : Bot
    {
        private Dictionary<string, SlashCommand> _slashCommands = new Dictionary<string, SlashCommand>();

        public AsciiCordBot(DiscordSocketClient client, string token, string jp2aPath)
            : base(client, token)
        {
            _slashCommands.Add("avatar", new AvatarSlashCommand(jp2aPath));
            _slashCommands.Add("image", new ImageSlashCommand(jp2aPath));
        }

        public override async Task RunAsync()
        {
            _client.Ready += ClientReady;
            _client.SlashCommandExecuted += SlashCommandHandler;
            await base.RunAsync();
            await Task.Delay(-1);
        }

        private async Task ClientReady()
        {
            try
            {
                var cmds = _slashCommands.Values.Select(c => c.GetSlashCommandBuilder().Build()).ToArray();
                await _client.BulkOverwriteGlobalApplicationCommandsAsync(cmds);

            }
            catch (HttpException)
            {

            }
        }

        private async Task SlashCommandHandler(SocketSlashCommand cmd)
        {
            await _slashCommands[cmd.Data.Name].Execute(cmd);
        }
    }
}
