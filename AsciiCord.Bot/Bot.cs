using Discord;
using Discord.WebSocket;

namespace AsciiCord.Bot
{
    public abstract class Bot
    {
        protected DiscordSocketClient _client;
        protected string _token;

        public Bot(DiscordSocketClient client, string token)
        {
            _client = client;
            _token = token;
        }

        public virtual async Task RunAsync()
        {
            _client.Log += Log;
            await _client.LoginAsync(TokenType.Bot, _token);
            await _client.StartAsync();
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}
