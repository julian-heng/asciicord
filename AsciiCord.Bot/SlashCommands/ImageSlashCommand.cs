using Discord;
using Discord.WebSocket;

using AsciiCord.Core;

namespace AsciiCord.Bot.SlashCommands
{
    public class ImageSlashCommand : AsciiSlashCommand
    {
        public override string Name => "image";
        public override string Description => "Renders an avatar in ascii";

        public ImageSlashCommand(string jp2aPath) : base(jp2aPath)
        {

        }

        protected override void _SetupSlashCommandBuilder(SlashCommandBuilder cmdBuilder)
        {
            cmdBuilder.AddOption("url", ApplicationCommandOptionType.String, "The image URL to render", isRequired: true);
        }

        protected override string GetSourceImageUrl(SocketSlashCommand cmd)
        {
            var url = cmd.Data.Options.FirstOrDefault(i => i.Name == "url")?.Value;
            return (string)url ?? string.Empty;
        }
    }
}
