using Discord;
using Discord.WebSocket;

namespace AsciiCord.Bot.SlashCommands
{
    public abstract class SlashCommand
    {
        public abstract string Name { get; }
        public abstract string Description { get; }

        public SlashCommandBuilder GetSlashCommandBuilder()
        {
            var cmdBuilder = new SlashCommandBuilder();
            cmdBuilder.WithName(Name);
            cmdBuilder.WithDescription(Description);
            _SetupSlashCommandBuilder(cmdBuilder);
            return cmdBuilder;
        }

        protected virtual void _SetupSlashCommandBuilder(SlashCommandBuilder cmdBuilder)
        {
        }

        public abstract Task Execute(SocketSlashCommand cmd);
    }
}
