using Discord;
using Discord.WebSocket;

using AsciiCord.Core;

namespace AsciiCord.Bot.SlashCommands
{
    public class AvatarSlashCommand : AsciiSlashCommand
    {
        public override string Name => "avatar";
        public override string Description => "Renders an avatar in ascii";

        public AvatarSlashCommand(string jp2aPath) : base(jp2aPath)
        {

        }

        protected override void _SetupSlashCommandBuilder(SlashCommandBuilder cmdBuilder)
        {
            cmdBuilder.AddOption("user", ApplicationCommandOptionType.User, "The user's avatar to render");
        }

        protected override string GetSourceImageUrl(SocketSlashCommand cmd)
        {
            var user = ((SocketUser)cmd.Data.Options.FirstOrDefault(i => i.Name == "user")?.Value) ?? cmd.User;
            var url = user.GetAvatarUrl();
            if (string.IsNullOrWhiteSpace(url))
            {
                url = user.GetDefaultAvatarUrl();
            }
            return url;
        }
    }
}
