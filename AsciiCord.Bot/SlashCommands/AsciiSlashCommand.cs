using Discord;
using Discord.WebSocket;

using AsciiCord.Core;

namespace AsciiCord.Bot.SlashCommands
{
    public abstract class AsciiSlashCommand : SlashCommand
    {
        private Jp2aWrapper _jp2a;

        public AsciiSlashCommand(string jp2aPath) : base()
        {
            _jp2a = new Jp2aWrapper(jp2aPath);
        }

        public async override Task Execute(SocketSlashCommand cmd)
        {
            var url = GetSourceImageUrl(cmd);
            var ascii = await _jp2a.ConvertUrlImageToAsciiAsync(url);
            var msg = string.IsNullOrWhiteSpace(ascii)
                    ? "Provided url is invalid."
                    : $"```ansi{Environment.NewLine}{ascii}{Environment.NewLine}```";
            await cmd.RespondAsync(msg);
        }

        protected abstract string GetSourceImageUrl(SocketSlashCommand cmd);
    }
}
