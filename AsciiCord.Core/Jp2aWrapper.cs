using System.Diagnostics;
using System.Text;

namespace AsciiCord.Core
{
    public class Jp2aWrapper
    {
        private static readonly string[] Jp2aArguments = new string[]
        {
            "--color", "--color-depth=4"
        };

        private string _jp2aPath;

        public Jp2aWrapper(string jp2aPath)
        {
            _jp2aPath = jp2aPath;
        }

        public async Task<string> ConvertUrlImageToAsciiAsync(string url)
        {
            var startInfo = new ProcessStartInfo
            {
                FileName = _jp2aPath,
                Arguments = string.Join(' ', Jp2aArguments.Concat(new string[] {
                    "--width=36",
                    url,
                })),
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                CreateNoWindow = true,
            };

            var process = new Process
            {
                StartInfo = startInfo,
            };

            process.Start();

            var output =  await process.StandardOutput.ReadToEndAsync();
            if (process.ExitCode != 0)
            {
                return string.Empty;
            }

            output = StripCodes(output);
            return output;
        }

        private string StripCodes(string str)
        {
            var sb = new StringBuilder();
            var currentCol = string.Empty;
            foreach (var line in str.Split(Environment.NewLine))
            {
                foreach (var cell in line.Split(Constants.AnsiReset))
                {
                    if (string.IsNullOrWhiteSpace(cell))
                    {
                        continue;
                    }

                    var pivot = cell.Length - 1;
                    var col = cell.Length == 1 ? Constants.AnsiReset : cell.Substring(0, pivot);
                    var c = cell.Length == 1 ? cell : cell.Substring(pivot, 1);

                    if (currentCol != col)
                    {
                        currentCol = col;
                        sb.Append(currentCol);
                    }
                    sb.Append(c);
                }
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
