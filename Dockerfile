FROM mcr.microsoft.com/dotnet/sdk as build-env
WORKDIR /app
COPY . /app
RUN dotnet restore
RUN dotnet publish --nologo --configuration Release

FROM mcr.microsoft.com/dotnet/runtime
RUN apt update \
    && apt install jp2a -y \
    && rm -rf /var/lib/apt/lists/*
WORKDIR /app
COPY --from=build-env /app/AsciiCord.Program/bin/Release/net6.0/publish /app
ENV TOKEN=""
CMD dotnet AsciiCord.Program.dll bot --token $TOKEN --jp2a-binary $(which jp2a)
