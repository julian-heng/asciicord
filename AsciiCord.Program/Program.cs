﻿using CommandLine;
using Discord.WebSocket;

using AsciiCord.Bot;

namespace AsciiCord.Program
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            await Parser.Default.ParseArguments<BotOptions>(args).MapResult(
                (BotOptions o) => RunBot(o.Token, o.Jp2aPath),
                e => Task.FromResult(1)
            );
        }

        static async Task RunBot(string token, string jp2aPath)
        {
            var socketClient = new DiscordSocketClient();
            var bot = new AsciiCordBot(socketClient, token, jp2aPath);
            await bot.RunAsync();
        }
    }
}
