using CommandLine;

namespace AsciiCord.Program
{
    public class CommandLineOptions
    {
        [Option('t', "token", Required = true, HelpText = "The bot token")]
        public string? Token { get; set; }
    }

    public class BotOptions : CommandLineOptions
    {
        [Option('b', "jp2a-binary", Required = false, HelpText = "jp2a binary path")]
        public string? Jp2aPath { get; set; } = "jp2a";
    }
}
